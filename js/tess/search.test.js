import * as tess from './search.js'

test('bad input', () => {
  const text = 'asdf'
  expect(tess.parseTessFile(text)).toEqual([])
})

// Latin tests

test('parse Latin', () => {
  const text = '<verg. aen. 1.3>	litora, multum ille et terris iactatus et alto\n' +
    '<verg. aen. 1.4>	vi superum saevae memorem Iunonis ob iram;\n'
  expect(tess.parseTessFile(text)).toEqual([
    {
      'tag': 'verg. aen. 1.3',
      'raw': 'litora, multum ille et terris iactatus et alto',
      'tokens': [
        {'start': 0, 'end': 6, 'seq': 'litora'},
        {'start': 8, 'end': 14, 'seq': 'multum'},
        {'start': 15, 'end': 19, 'seq': 'ille'},
        {'start': 20, 'end': 22, 'seq': 'et'},
        {'start': 23, 'end': 29, 'seq': 'terris'},
        {'start': 30, 'end': 38, 'seq': 'iactatus'},
        {'start': 39, 'end': 41, 'seq': 'et'},
        {'start': 42, 'end': 46, 'seq': 'alto'}
      ]
    },
    {
      'tag': 'verg. aen. 1.4',
      'raw': 'vi superum saevae memorem Iunonis ob iram;',
      'tokens': [
        {'start': 0, 'end': 2, 'seq': 'vi'},
        {'start': 3, 'end': 10, 'seq': 'superum'},
        {'start': 11, 'end': 17, 'seq': 'saevae'},
        {'start': 18, 'end': 25, 'seq': 'memorem'},
        {'start': 26, 'end': 33, 'seq': 'Iunonis'},
        {'start': 34, 'end': 36, 'seq': 'ob'},
        {'start': 37, 'end': 41, 'seq': 'iram'}
      ]
    }
  ])
})


test('normalize Latin', () => {
  expect(tess.normalizedLatin('adjuvant')).toEqual('adiuuant')
})


// Greek tests

test('parse Greek', () => {
  const text = '<theocritus. idylls. 1.35> νεικείουσ᾽ ἐπέεσσι. τὰ δ᾽ οὐ φρενὸς ἅπτεται αὐτᾶς:\n' +
    '<theocritus. idylls. 1.36> ἀλλ᾽ ὁκὰ μὲν τῆνον ποτιδέρκεται ἄνδρα γελᾶσα,\n'
  expect(tess.parseTessFile(text)).toEqual([
    {
      'tag': 'theocritus. idylls. 1.35',
      'raw': 'νεικείουσ᾽ ἐπέεσσι. τὰ δ᾽ οὐ φρενὸς ἅπτεται αὐτᾶς:',
      'tokens': [
        {'start': 0, 'end': 9, 'seq': 'νεικείουσ'},
        {'start': 11, 'end': 18, 'seq': 'ἐπέεσσι'},
        {'start': 20, 'end': 22, 'seq': 'τὰ'},
        {'start': 23, 'end': 24, 'seq': 'δ'},
        {'start': 26, 'end': 28, 'seq': 'οὐ'},
        {'start': 29, 'end': 35, 'seq': 'φρενὸς'},
        {'start': 36, 'end': 43, 'seq': 'ἅπτεται'},
        {'start': 44, 'end': 49, 'seq': 'αὐτᾶς'}
      ]
    },
    {
      'tag': 'theocritus. idylls. 1.36',
      'raw': 'ἀλλ᾽ ὁκὰ μὲν τῆνον ποτιδέρκεται ἄνδρα γελᾶσα,',
      'tokens': [
        {'start': 0, 'end': 3, 'seq': 'ἀλλ'},
        {'start': 5, 'end': 8, 'seq': 'ὁκὰ'},
        {'start': 9, 'end': 12, 'seq': 'μὲν'},
        {'start': 13, 'end': 18, 'seq': 'τῆνον'},
        {'start': 19, 'end': 31, 'seq': 'ποτιδέρκεται'},
        {'start': 32, 'end': 37, 'seq': 'ἄνδρα'},
        {'start': 38, 'end': 44, 'seq': 'γελᾶσα'}
      ]
    }
  ])
})


test('normalize Greek', () => {
  expect(tess.normalizedGreek('ἄνθρωπός')).toEqual('ἄνθρωπος')
})


// general tests
test('calculate frequencies', () => {
  const features = [
    {'repr': 'a', 'parsedPos': 0, 'tokenPos':0},
    {'repr': 'a', 'parsedPos': 0, 'tokenPos':0},
    {'repr': 'a', 'parsedPos': 0, 'tokenPos':0},
    {'repr': 'a', 'parsedPos': 0, 'tokenPos':0},
    {'repr': 'a', 'parsedPos': 0, 'tokenPos':0},
    {'repr': 'b', 'parsedPos': 0, 'tokenPos':0},
    {'repr': 'b', 'parsedPos': 0, 'tokenPos':0},
    {'repr': 'b', 'parsedPos': 0, 'tokenPos':0},
    {'repr': 'b', 'parsedPos': 0, 'tokenPos':0},
    {'repr': 'c', 'parsedPos': 0, 'tokenPos':0},
    {'repr': 'c', 'parsedPos': 0, 'tokenPos':0}
  ]
  expect(tess.calculateFrequencies(features)).toEqual({
    'a': 5.0 / 11.0,
    'b': 4.0 / 11.0,
    'c': 2.0 / 11.0
  })
})


test('build stopwords', () => {
  const freqs = {
    'a': 5.0 / 11.0,
    'b': 4.0 / 11.0,
    'c': 2.0 / 11.0
  }
  expect(tess.buildStopwords(1, freqs, freqs)).toEqual({'a': true})
})


test('featurify', () => {
  const parsed = [
    {'tag': '0', 'raw': 'a a a b', 'tokens': [
      {'start': 0, 'end': 1, 'seq': 'a'},
      {'start': 2, 'end': 3, 'seq': 'a'},
      {'start': 4, 'end': 5, 'seq': 'a'},
      {'start': 6, 'end': 7, 'seq': 'b'}
    ]},
    {'tag': '1', 'raw': 'b b a b', 'tokens': [
      {'start': 0, 'end': 1, 'seq': 'b'},
      {'start': 2, 'end': 3, 'seq': 'b'},
      {'start': 4, 'end': 5, 'seq': 'a'},
      {'start': 6, 'end': 7, 'seq': 'b'}
    ]}
  ]

  expect(tess.featurify(parsed, token => [token])).toEqual([
    {'repr': 'a', 'parsedPos': 0, 'tokenPos': 0},
    {'repr': 'a', 'parsedPos': 0, 'tokenPos': 1},
    {'repr': 'a', 'parsedPos': 0, 'tokenPos': 2},
    {'repr': 'b', 'parsedPos': 0, 'tokenPos': 3},
    {'repr': 'b', 'parsedPos': 1, 'tokenPos': 0},
    {'repr': 'b', 'parsedPos': 1, 'tokenPos': 1},
    {'repr': 'a', 'parsedPos': 1, 'tokenPos': 2},
    {'repr': 'b', 'parsedPos': 1, 'tokenPos': 3},
  ])

  expect(tess.featurify(parsed, token => {return {'a': 'c', 'b': 'd'}[token]})).toEqual([
    {'repr': 'c', 'parsedPos': 0, 'tokenPos': 0},
    {'repr': 'c', 'parsedPos': 0, 'tokenPos': 1},
    {'repr': 'c', 'parsedPos': 0, 'tokenPos': 2},
    {'repr': 'd', 'parsedPos': 0, 'tokenPos': 3},
    {'repr': 'd', 'parsedPos': 1, 'tokenPos': 0},
    {'repr': 'd', 'parsedPos': 1, 'tokenPos': 1},
    {'repr': 'c', 'parsedPos': 1, 'tokenPos': 2},
    {'repr': 'd', 'parsedPos': 1, 'tokenPos': 3},
  ])

  expect(tess.featurify(parsed, token => {return {'a': ['1', '2'], 'b': ['3', '4']}[token]})).toEqual([
    {'repr': '1', 'parsedPos': 0, 'tokenPos': 0},
    {'repr': '2', 'parsedPos': 0, 'tokenPos': 0},
    {'repr': '1', 'parsedPos': 0, 'tokenPos': 1},
    {'repr': '2', 'parsedPos': 0, 'tokenPos': 1},
    {'repr': '1', 'parsedPos': 0, 'tokenPos': 2},
    {'repr': '2', 'parsedPos': 0, 'tokenPos': 2},
    {'repr': '3', 'parsedPos': 0, 'tokenPos': 3},
    {'repr': '4', 'parsedPos': 0, 'tokenPos': 3},
    {'repr': '3', 'parsedPos': 1, 'tokenPos': 0},
    {'repr': '4', 'parsedPos': 1, 'tokenPos': 0},
    {'repr': '3', 'parsedPos': 1, 'tokenPos': 1},
    {'repr': '4', 'parsedPos': 1, 'tokenPos': 1},
    {'repr': '1', 'parsedPos': 1, 'tokenPos': 2},
    {'repr': '2', 'parsedPos': 1, 'tokenPos': 2},
    {'repr': '3', 'parsedPos': 1, 'tokenPos': 3},
    {'repr': '4', 'parsedPos': 1, 'tokenPos': 3},
  ])
})


test('indexFeatures', () => {
  const features = [
    {'repr': 'a', 'parsedPos': 0, 'tokenPos': 0},
    {'repr': 'a', 'parsedPos': 0, 'tokenPos': 1},
    {'repr': 'a', 'parsedPos': 0, 'tokenPos': 2},
    {'repr': 'b', 'parsedPos': 0, 'tokenPos': 3},
    {'repr': 'b', 'parsedPos': 1, 'tokenPos': 0},
    {'repr': 'b', 'parsedPos': 1, 'tokenPos': 1},
    {'repr': 'a', 'parsedPos': 1, 'tokenPos': 2},
    {'repr': 'b', 'parsedPos': 1, 'tokenPos': 3},
  ]

  expect(tess.indexFeatures(features)).toEqual({
    'a': [
      {'repr': 'a', 'parsedPos': 0, 'tokenPos': 0},
      {'repr': 'a', 'parsedPos': 0, 'tokenPos': 1},
      {'repr': 'a', 'parsedPos': 0, 'tokenPos': 2},
      {'repr': 'a', 'parsedPos': 1, 'tokenPos': 2}
    ],
    'b': [
      {'repr': 'b', 'parsedPos': 0, 'tokenPos': 3},
      {'repr': 'b', 'parsedPos': 1, 'tokenPos': 0},
      {'repr': 'b', 'parsedPos': 1, 'tokenPos': 1},
      {'repr': 'b', 'parsedPos': 1, 'tokenPos': 3}
    ]
  })
})


test('compareFeatures', () => {
  const simpleSourceFeatures = [
    {'repr': 'a', 'parsedPos': 0, 'tokenPos': 0},
    {'repr': 'b', 'parsedPos': 0, 'tokenPos': 1},
    {'repr': 'c', 'parsedPos': 0, 'tokenPos': 2},
  ]

  const simpleTargetFeatures = [
    {'repr': 'a', 'parsedPos': 0, 'tokenPos': 0},
    {'repr': 'b', 'parsedPos': 0, 'tokenPos': 1},
  ]

  expect(tess.compareFeatures(simpleSourceFeatures, simpleTargetFeatures, {})).toEqual([
    {
      'sourceParsedPos': 0,
      'targetParsedPos': 0,
      'matches': [
        {'sourceTokenPos': 0, 'targetTokenPos': 0, 'reprs': {'a': true}},
        {'sourceTokenPos': 1, 'targetTokenPos': 1, 'reprs': {'b': true}}
      ]
    }
  ])
})


test('score line pair', () => {
  const context = {
    'sourceFrequencies': {'a': 0.1, 'b': 0.2, 'c': 0.65, 'd': 0.05},
    'targetFrequencies': {'a': 0.3, 'b': 0.1, 'c': 0.45, 'd': 0.15}
  }
  const linePair = {
    'sourceParsedPos': 0,
    'targetParsedPos': 0,
    'matches': [
      {'sourceTokenPos': 0, 'targetTokenPos': 0, 'reprs': {'a': true}},
      {'sourceTokenPos': 1, 'targetTokenPos': 1, 'reprs': {'b': true}}
    ]
  }

  expect(tess.scoreLinePair(linePair, context)).toBeCloseTo(2.65089178726)
})


test('calculateDistance', () => {
  const posReprsPair = [
    {'pos': 0, 'reprs': {'a': true, 'b': true}},
    {'pos': 3, 'reprs': {'c': true}}
  ]

  const freqs = {'a': 0.1, 'b': 0.3, 'c': 0.45, 'd': 0.15}

  expect(tess.calculateDistance(posReprsPair, freqs)).toEqual(3)

  const posReprs = [
    {'pos': 0, 'reprs': {'a': true, 'b': true}},
    {'pos': 1, 'reprs': {'d': true}},
    {'pos': 3, 'reprs': {'c': true}}
  ]

  expect(tess.calculateDistance(posReprs, freqs)).toEqual(1)
})
