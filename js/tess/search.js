import {Unistring} from './unistring.js'


export function calculateFrequencies(features) {
  var result = {}
  for (const feat of features) {
    const repr = feat['repr']
    if (repr in result) {
      result[repr] += 1
    } else {
      result[repr] = 1
    }
  }
  for (const repr in result) {
    result[repr] /= 1.0*features.length
  }
  return result
}


export function buildStopwords(count, sourceFrequencies, targetFrequencies) {
  var tmp = []
  for (const repr in sourceFrequencies) {
    if (repr in targetFrequencies) {
      const sourceFreq = sourceFrequencies[repr]
      const targetFreq = targetFrequencies[repr]
      tmp.push([repr, (sourceFreq + targetFreq) / 2.0])
    }
  }
  tmp.sort((a, b) => b[1] - a[1])
  var result = {}
  for (var i = 0; i < count; i++) {
    result[tmp[i][0]] = true
  }
  return result
}


// apparently, these are all of the combining diacriticals according to Unicode
const diacriticals = /[\u0300-\u036F\u1DC0-\u1DFF\u20D0-\u20FF]/ug


export function normalizedLatin(str) {
  return str
    .normalize('NFKD')
    .toLowerCase()
    .replace(diacriticals, '')
    .replace(/[0-9]/g, '')
    .replace(/\W/ug, '')
    .replace(/j/g, 'i')
    .replace(/v/g, 'u')
}


// removes extra acute due to an enclitic
function normGrkAcuteEnclitic(partial) {
  // looking for acutes and circumflexes
  const matches = partial.match(/[\u{0301}\u{0302}]/gu)
  if (matches.length > 1) {
    // there was an extra acute, we remove it
    for (var i = partial.length - 1; i >= 0; i--) {
      if (partial[i] === '\u0301') {
        return partial.substring(0, i) + partial.substring(i + 1)
      }
    }
  }
  return partial
}


export function normalizedGreek(str) {
  const result = str
    .normalize('NFKD')
    .toLowerCase()
    .replace(/[0-9]/g, '')
    // change grave to acute
    .replace(/\u{0300}/ug, '\u{0301}')
    // NB:  sigma handling different from v3;
    .replace(/σ/g, 'ς')
  const tmp = normGrkAcuteEnclitic(result)

  // for some reason, this strips the alpha off in the test
  // return tmp.replace(/\W/u, '')
  return tmp
}


/*
 * String -> [Token]
 *
 * Token :: {
 *   'start': int,
 *   'end': int,
 *   'seq': String
 * }
 */
export function buildTokenSequence(text, norm) {
  const words = Unistring.getWords(text)
  return words
    .filter(word => word['type'] === Unistring.WBP['ALetter'])
    .map(word => {
      return {
        'start': word['index'],
        'end': word['index'] + word['length'],
        'seq': norm(word['text'])
      }
    })
}


/*
 * .tess -> [Parsed]
 *
 * Parsed :: {
 *   'tag': String,
 *   'raw': String,
 *   'tokens': [Token]
 * }
 */
export function parseTessFile(text, norm) {
  const lines = text.split(/[\n]/)
  return lines
    .filter(line => /^\S*<(.+)>\s+(.+)$/.test(line))
    .map(line => {
      const regexed = /^\S*<(.+)>\s+(.+)$/.exec(line)
      const tag = regexed[1]
      const raw = regexed[2]
      return {
        'tag': tag,
        'raw': raw,
        'tokens': buildTokenSequence(raw, norm)
      }
    })
}


export function identity(seq) {
  return [seq]
}


export function lookupClosure(lookup) {
  function inner(seq) {
    if (seq in lookup) {
      var result = []
      for (const repr in lookup[seq]) {
        result.push(repr)
      }
      return result
    }
    return []
  }
  return inner
}


/*
 * ([Parsed], String -> [String]) -> [Feature]
 *
 * Feature :: {
 *   'repr': String,
 *   'parsedPos': int,
 *   'tokenPos': int
 * }
 */
export function featurify(parsed, transform) {
  var result = []
  for (var i = 0; i < parsed.length; i++) {
    for (var pos = 0; pos < parsed[i]['tokens'].length; pos++) {
      const transformed = transform(parsed[i]['tokens'][pos]['seq'])
      if (transformed.length > 0) {
        for (const repr of transformed) {
          result.push({
            'repr': repr,
            'parsedPos': i,
            'tokenPos': pos
          })
        }
      }
    }
  }
  return result
}


/*
 * [Feature] -> {String: [Feature]}
 */
export function indexFeatures(features) {
  var result = {}
  for (const feature of features) {
    const repr = feature['repr']
    if (repr in result) {
      result[repr].push(feature)
    } else {
      result[repr] = [feature]
    }
  }
  return result
}


/*
 * ([Features], [Features], {String: Boolean}) -> [LinePair]
 *
 * LinePair :: {
 *    'sourceParsedPos': int,
 *    'targetParsedPos': int,
 *    'matches': [Match]
 * }
 *
 * Match :: {
 *    'sourceTokenPos': int,
 *    'targetTokenPos': int,
 *    'reprs': {String: Boolean}
 * }
 */
export function compareFeatures(sourceFeatures, targetFeatures, stopwords) {
  const sourceIndex = indexFeatures(sourceFeatures)
  var t_parsedPos2s_parsedPos2t_tokenPos2s_tokenPos2reprs = new Map()
  for (const targetFeat of targetFeatures) {
    const targetFeatRepr = targetFeat['repr']
    if (targetFeatRepr in sourceIndex) {
      for (const sourceFeat of sourceIndex[targetFeatRepr]) {
        const targetFeatParsedPos = targetFeat['parsedPos']
        if (!t_parsedPos2s_parsedPos2t_tokenPos2s_tokenPos2reprs.has(targetFeatParsedPos)) {
          t_parsedPos2s_parsedPos2t_tokenPos2s_tokenPos2reprs.set(targetFeatParsedPos, new Map())
        }
        const s_parsedPos2t_tokenPos2s_tokenPos2reprs = t_parsedPos2s_parsedPos2t_tokenPos2s_tokenPos2reprs.get(targetFeatParsedPos)
        const sourceFeatParsedPos = sourceFeat['parsedPos']
        if (!s_parsedPos2t_tokenPos2s_tokenPos2reprs.has(sourceFeatParsedPos)) {
          s_parsedPos2t_tokenPos2s_tokenPos2reprs.set(sourceFeatParsedPos, new Map())
        }
        const t_tokenPos2s_tokenPos2reprs = s_parsedPos2t_tokenPos2s_tokenPos2reprs.get(sourceFeatParsedPos)
        const targetFeatTokenPos = targetFeat['tokenPos']
        if (!t_tokenPos2s_tokenPos2reprs.has(targetFeatTokenPos)) {
          t_tokenPos2s_tokenPos2reprs.set(targetFeatTokenPos, new Map())
        }
        const s_tokenPos2reprs = t_tokenPos2s_tokenPos2reprs.get(targetFeatTokenPos)
        const sourceFeatTokenPos = sourceFeat['tokenPos']
        if (!s_tokenPos2reprs.has(sourceFeatTokenPos)) {
          s_tokenPos2reprs.set(sourceFeatTokenPos, {})
        }
        const reprs = s_tokenPos2reprs.get(sourceFeatTokenPos)
        reprs[targetFeatRepr] = true
      }
    }
  }

  var result = []
  for (const [targetFeatParsedPos, s_parsedPos2t_tokenPos2s_tokenPos2reprs] of t_parsedPos2s_parsedPos2t_tokenPos2s_tokenPos2reprs) {
    for (const [sourceFeatParsedPos, t_tokenPos2s_tokenPos2reprs] of s_parsedPos2t_tokenPos2s_tokenPos2reprs) {
      var tmp = []
      for (const [targetFeatTokenPos, s_tokenPos2reprs] of t_tokenPos2s_tokenPos2reprs) {
        for (const [sourceFeatTokenPos, reprs] of s_tokenPos2reprs) {
          for (const repr in reprs) {
            if (repr in stopwords === false) {
              tmp.push({
                'sourceTokenPos': sourceFeatTokenPos,
                'targetTokenPos': targetFeatTokenPos,
                'reprs': reprs
              })
              break
            }
          }
        }
      }
      result.push({
        'sourceParsedPos': sourceFeatParsedPos,
        'targetParsedPos': targetFeatParsedPos,
        'matches': tmp
      })
    }
  }
  return result
}


function calculateScoreNumerator(matchReprs, freqs) {
  return matchReprs
    .map(repr => 1.0 / freqs[repr])
    .reduce((acc, cur) => acc + cur, 0.0)
}


function calculateSourceDistance(matches, sourceFreqs) {
  const posReprs = matches
    .map(match => {return {'pos': match['sourceTokenPos'], 'reprs': match['reprs']}})
  return calculateDistance(posReprs, sourceFreqs)
}


function calculateTargetDistance(matches, targetFreqs) {
  const posReprs = matches
    .map(match => {return {'pos': match['targetTokenPos'], 'reprs': match['reprs']}})
  return calculateDistance(posReprs, targetFreqs)
}


function getPositions(posReprs, repr) {
  return posReprs
    .filter(posRepr => repr in posRepr['reprs'])
    .map(posRepr => posRepr['pos'])
}


/*
 * ([PosRepr], {String: float}) -> int
 *
 * PosRepr :: {
 *   'pos': int,
 *   'reprs': {String: Boolean}
 * }
 */
export function calculateDistance(posReprs, freqs) {
  if (posReprs.length === 2) {
    if (posReprs[0]['pos'] < posReprs[1]['pos']) {
      return posReprs[1]['pos'] - posReprs[0]['pos']
    }
    return posReprs[0]['pos'] - posReprs[1]['pos']
  }

  var reprFreqs = []
  for (const posRepr of posReprs) {
    for (const repr in posRepr['reprs']) {
      if (repr in freqs) {
        reprFreqs.push([repr, freqs[repr]])
      }
    }
  }

  const sortedReprFreqs = reprFreqs.sort((a, b) => a[1] - b[1])
  const lowestPositions = getPositions(posReprs, sortedReprFreqs[0][0])
  const penlowestPositions = getPositions(posReprs, sortedReprFreqs[1][0])

  var crossProduct = []
  for (const lowestPos of lowestPositions) {
    for (const penlowestPos of penlowestPositions) {
      crossProduct.push([lowestPos, penlowestPos])
    }
  }

  const distances = crossProduct.map(item => Math.abs(item[0], item[1]))
  var largest = 1
  for (const dist of distances) {
    if (dist > largest) {
      largest = dist
    }
  }
  return largest
}


/*
 * (LinePair, Context) -> float
 *
 * Context :: {
 *   'sourceFrequencies': {String: float},
 *   'targetFrequencies': {String: float}
 * }
 */
export function scoreLinePair(linePair, context) {
  if (linePair['matches'].length < 2) return 0.0
  // TODO handle multiple ut
  const matchReprs = linePair['matches']
    .map(match => Object.keys(match['reprs']))
    .reduce((acc, cur) => acc.concat(cur), [])
  const sourceNum = calculateScoreNumerator(matchReprs, context['sourceFrequencies'])
  const targetNum = calculateScoreNumerator(matchReprs, context['targetFrequencies'])
  const sourceDist = calculateSourceDistance(linePair['matches'], context['sourceFrequencies'])
  const targetDist = calculateTargetDistance(linePair['matches'], context['targetFrequencies'])
  return Math.log((sourceNum + targetNum) / (sourceDist + targetDist))
}
