"""Generates lookup dictionary files"""
import argparse
import json
import os
import regex as re
import unicodedata


DIACRITICS = re.compile(r'[\u0300-\u036F\u1DC0-\u1DFF\u20D0-\u20FF]')
ACUTE_CIRCUMFLEX = re.compile(r'[\u0301\u0302]')
DIGITS = re.compile(r'\d+')
NON_WORDS = re.compile(r'\W+')
NUMBERS = re.compile(r'[0-9]')


def _parse_args():
    """Parses command line arguments"""
    parser = argparse.ArgumentParser(description='Template file')
    parser.add_argument(
        'csv_dir',
        help='path to csv lexicon files')
    return parser.parse_args()


def normalizedLatin(text):
    """Normalizes text per Latin rules"""
    no_digits = DIGITS.sub('', text)
    decomposed = unicodedata.normalize('NFKD', no_digits)
    no_diacritics = DIACRITICS.sub('', decomposed)
    lower = no_diacritics.lower()
    j2i = lower.replace('j', 'i')
    v2u = j2i.replace('v', 'u')
    return NON_WORDS.sub('', v2u)


def normalizedGreek(text):
    """Normalizes text per Greek rules"""
    no_digits = DIGITS.sub('', text)
    decomposed = unicodedata.normalize('NFKD', no_digits)
    lower = decomposed.lower()
    # change grave to acute
    acuted = lower.replace('\u0300', '\u0301')
    # NB:  sigma handling different from v3
    sigmaed = acuted.replace('σ', 'ς')
    # handle extra acute due to enclitic, if present
    matches = ACUTE_CIRCUMFLEX.findall(sigmaed)
    if len(matches) > 1:
        pos = sigmaed.rfind('\u0301')
        return NON_WORDS.sub('', sigmaed[:pos] + sigmaed[pos:])
    return NON_WORDS.sub('', sigmaed)


def generateLookup(csv_path, normalizer):
    """Generates lookup dictionary as JSON"""
    result = {}
    with open(csv_path) as ifh:
        for line in ifh:
            entries = line.split(',')
            if len(entries) < 3:
                continue
            morphed = normalizer(entries[0])
            lemma = normalizer(entries[2])
            if morphed and lemma:
                if morphed in result:
                    result[morphed][lemma] = True
                else:
                    result[morphed] = {lemma: True}
    return result


def _run():
    """Displays input command line arguments"""
    args = _parse_args()
    latin_lookup = generateLookup(
        os.path.join(args.csv_dir, 'la.lexicon.csv'),
        normalizedLatin,
    )
    greek_lookup = generateLookup(
        os.path.join(args.csv_dir, 'grc.lexicon.csv'),
        normalizedGreek,
    )
    with open('lookup.js', 'w') as ofh:
        ofh.write('export const LatinLookup = ')
        ofh.write(json.dumps(latin_lookup))
        ofh.write('\n')
        ofh.write('export const GreekLookup = ')
        ofh.write(json.dumps(greek_lookup))
        ofh.write('\n')


if __name__ == '__main__':
    _run()
