# Tesserae Javascript

This is an implementation of the Tesserae algorithm in Javascript.  It is conceived of as a way to see whether putting comparison computation on the client is feasible.

## Running Code

Build `lookup.js` by running

`python3 lookup.py <path to csv directory>`

where `path to csv directory` refers to the directory where Tesserae's lexicon csv files are stored.  If you clone the [Tesserae repository](https://github.com/tesserae/tesserae), then the directory where the lexicon csv files are stored is `<path to cloned directory>/data/common/`.

Start a web server with its root pointed to this directory.  If you have Python 3 installed, this should be as easy as running `python3 -m http.server` in this directory.

Launch your browser (I've been using Chromium 63).  Direct your browser to the appropriate URL ([`localhost:8000`](localhost:8000) if you're running the Python 3 web server).

Enter .tess files into the text areas and hit the "Submit" button.  The results should be populated on that page.

## Development

### Prerequisites

Although possible for this code to run correctly in many different environments, here's the one I use (and the only one I can guarantee this code working in):
* Ubuntu 16.04
* Node v8.9.4
* yarn 1.3.2

### Installing Dependencies

In this directory, run:
`yarn install`

### Workflow

All of the functions are exported from `tess.js`.  For ensuring that the functions are performing correctly, unit tests are written in `tess.test.js`.  To run the tests, invoke `jest`.

During development, you can leave `jest` running, watching for file changes to re-run unit tests, with `jest --watch`; I've also bound `npm test` to this command for this project.

