import * as tess from './tess/search.js'
import {LatinLookup, GreekLookup} from './lookup.js'


// interface between web page and comparison code
export function handleSubmission() {
  const totalStart = performance.now()
  // new submission means new results
  document.getElementById('outputArea').innerHTML = ''

  const sourceText = document.getElementById('sourceText').value
  const targetText = document.getElementById('targetText').value

  const computeStart = performance.now()
  const normalize = tess.normalizedLatin
  const parsedSource = tess.parseTessFile(sourceText, normalize)
  //console.log('Parsed Source (' + Math.round(performance.now() - computeStart) / 1000.0 + ')')
  const parsedTarget = tess.parseTessFile(targetText, normalize)
  //console.log('Parsed Target (' + Math.round(performance.now() - computeStart) / 1000.0 + ')')
  const lookupLatin = tess.lookupClosure(LatinLookup)
  const sourceFeatures = tess.featurify(parsedSource, lookupLatin)
  //console.log('Featurified Source (' + Math.round(performance.now() - computeStart) / 1000.0 + ')')
  const targetFeatures = tess.featurify(parsedTarget, lookupLatin)
  //console.log('Featurified Target (' + Math.round(performance.now() - computeStart) / 1000.0 + ')')
  const sourceFrequencies = tess.calculateFrequencies(sourceFeatures)
  //console.log('Frequencified Source (' + Math.round(performance.now() - computeStart) / 1000.0 + ')')
  const targetFrequencies = tess.calculateFrequencies(targetFeatures)
  // printFrequencies(targetFrequencies)
  //console.log('Frequencified Target (' + Math.round(performance.now() - computeStart) / 1000.0 + ')')
  const stopwords = tess.buildStopwords(10, sourceFrequencies, targetFrequencies)
  //console.log('Got stopwords (' + Math.round(performance.now() - computeStart) / 1000.0 + ')')
  const linePairs = tess.compareFeatures(sourceFeatures, targetFeatures, stopwords)
  //console.log('Computed line pairs (' + Math.round(performance.now() - computeStart) / 1000.0 + ')')
  const context = {
    'sourceFrequencies': sourceFrequencies,
    'targetFrequencies': targetFrequencies
  }
  const results = linePairs
    .map(linePair => [linePair, tess.scoreLinePair(linePair, context)])
    .filter(item => item[1] > 0)
    .sort((a, b) => b[1] - a[1])
  const computeEnd = performance.now()
  console.log('Time to compute: ' + Math.round(computeEnd - computeStart) / 1000.0 + ' seconds')

  const displayStart = performance.now()
  const formattedStopwords = formatStopwords(stopwords)
  const formattedResults = results
    .map(result => formatScoredResult(result[0], result[1], parsedSource, parsedTarget))
    .join('')
  //console.log('Formatted size: ' + formattedResults.length)
  //console.log('Built formatted results (' + Math.round(performance.now() - displayStart) / 1000.0 + ')')
  document.getElementById('outputArea').innerHTML = `<p>Stopwords: ${formattedStopwords}</p><table>
<tr><th>Source</th><th>Phrase</th><th>Target</th><th>Phrase</th><th>Matched on</th><th>Score</th></tr>${formattedResults}</table>`
  const displayEnd = performance.now()
  console.log('Time to display: ' + Math.round(displayEnd - displayStart) / 1000.0 + ' seconds')
  console.log('Total time: ' + Math.round(performance.now() - totalStart) / 1000.0 + ' seconds')
}


function formatStopwords(stopwords) {
  var stopwordsArray = []
  for (const stopword in stopwords) {
    stopwordsArray.push(stopword)
  }
  stopwordsArray.sort()
  return stopwordsArray.join(', ')
}


function formatScoredResult(linePair, score, parsedSource, parsedTarget) {
  const sourceParsedPos = linePair['sourceParsedPos']
  const targetParsedPos = linePair['targetParsedPos']
  return `<tr><td>${parsedSource[sourceParsedPos]['tag']}</td>
<td>${parsedSource[sourceParsedPos]['raw']}</td>
<td>${parsedTarget[targetParsedPos]['tag']}</td>
<td>${parsedTarget[targetParsedPos]['raw']}</td>
<td>${linePair['matches'].map(match => Object.keys(match['reprs']).sort().join('-')).join(', ')}</td>
<td>${score}</td></tr>`
}


function printFrequencies(freqs) {
  var tmp = []
  for (const repr in freqs) {
    tmp.push([repr, freqs[repr]])
  }
  tmp.sort((a, b) => b[1] - a[1])
  for (const item of tmp) {
    console.log(item[0] + "\t" + item[1])
  }
}
