#include<cstring>
#include<iostream>
#include<memory>
#include<sstream>
#include<string>
#include<unordered_set>

#include<unicode/brkiter.h>
#include<unicode/regex.h>
#include<unicode/translit.h>
#include<unicode/unistr.h>

#include<zlib.h>

void check_greek() {
    UErrorCode status = U_ZERO_ERROR;
    RegexMatcher parser("^\\S*<(.+)>\\s+(.+)", 0, status);
    UnicodeString check = UnicodeString::fromUTF8(StringPiece("<pind. i. 1.1> μᾶτερ ἐμά, τὸ τεόν, χρύσασπι Θήβα,"));
    parser.reset(check);
    if (parser.matches(status)) {
        std::string got;
        parser.group(1, status).toUTF8String(got);
        std::cout << got << std::endl;
    }
}

void check_translit() {
    UParseError parse_status;
    UErrorCode status = U_ZERO_ERROR;
    // Documentation for building transliterator rules:
    // http://userguide.icu-project.org/transforms/general
    std::unique_ptr<Transliterator> converter(Transliterator::createFromRules(
        "Latin-Classical", ":: lower; j > i; v > u;", UTRANS_FORWARD, parse_status, status));

    std::string before("Verum Jacit");
    std::cout << before << std::endl;
    UnicodeString check = UnicodeString::fromUTF8(StringPiece(before));
    converter->transliterate(check);
    std::string result;
    check.toUTF8String(result);
    std::cout << result << std::endl;
}

void list_translit_ids() {
    UErrorCode status = U_ZERO_ERROR;
    std::unique_ptr<StringEnumeration> available(Transliterator::getAvailableIDs(status));

    while(const UnicodeString* cur = available->snext(status)) {
        std::string result;
        cur->toUTF8String(result);
        std::cout << result << std::endl;
    }
}

/**
 * Makes grapheme counting easier
 *
 * ICU documentation suggests not making too many BreakIterators, which is
 * what this class wraps
 */
class GraphemeCounter {
    // maintainer beware:  declaration order of members is important to
    // constructor
    UErrorCode my_status;
    std::unique_ptr<BreakIterator> my_iter;

public:
    GraphemeCounter(const Locale& where):
        my_status(U_ZERO_ERROR),
        my_iter(BreakIterator::createCharacterInstance(where, this->my_status)){}

    size_t operator()(const UnicodeString& text) {
        this->my_iter->setText(text);
        int32_t start_boundary = this->my_iter->first();
        if (start_boundary == BreakIterator::DONE) {
            return 0;
        }
        size_t result = 0;
        // assuming that if there is a start boundary, then there will be
        // an end boundary
        int32_t end_boundary = this->my_iter->next();
        while (end_boundary != BreakIterator::DONE) {
            ++result;
            start_boundary = end_boundary;
            end_boundary = this->my_iter->next();
        }
        return result;
    }
};

void check_graph_counter() {
    UnicodeString check("food");
    GraphemeCounter counter(Locale::getUS());
    std::cout << counter(check) << std::endl;
}

template<typename U>
auto crazy(U& arg) {
    UnicodeString check("food");
    return arg(check);
};

struct Weird {
    std::string operator()(const UnicodeString& text) {
        std::string result;
        text.toUTF8String(result);
        return result;
    }
};

void check_crazy() {
    GraphemeCounter grapheme_counter(Locale::getUS());
    auto result = crazy(grapheme_counter);
    std::cout << result << std::endl;
    Weird weird{};
    auto result2 = crazy(weird);
    std::cout << result2 << std::endl;
}

void try_compress() {
    uLong compr_len = 10000 * sizeof(int);
    ulong uncompr_len = compr_len;
    Byte* compr = (Byte*)calloc((uInt)compr_len, 1);
    Byte* uncompr = (Byte*)calloc((uInt)uncompr_len, 1);

    const char* hello{"Hello Hello Hello Hello Hello Hello Hello Hello"};
    uLong len = (uLong)strlen(hello) + 1;

    int err = compress(compr, &compr_len, (const Bytef*)hello, len);
    if (err != Z_OK) {
        std::stringstream msg;
        msg << "Compression error: " << err;
        throw std::runtime_error(msg.str());
    }

    strcpy((char*)uncompr, "garbage");

    err = uncompress(uncompr, &uncompr_len, compr, compr_len);
    if (err != Z_OK) {
        std::stringstream msg;
        msg << "Uncompression error: " << err;
        throw std::runtime_error(msg.str());
    }

    std::cout << "Compressed size: " << compr_len << std::endl;
    std::cout << "Uncompressed: " << (char*) uncompr << std::endl;

    free(compr);
    free(uncompr);
}

// https://stackoverflow.com/questions/10626856/how-to-split-a-tuple
template < std::size_t... Ns , typename... Ts >
auto tail_impl( std::index_sequence<Ns...> , std::tuple<Ts...> t )
{
   return  std::make_tuple( std::get<Ns+1u>(t)... );
}

template < typename... Ts >
auto tail( std::tuple<Ts...> t )
{
   return  tail_impl( std::make_index_sequence<sizeof...(Ts) - 1u>() , t );
}

void try_tuple_tails() {
    auto check = std::make_tuple('a', 1, 50.6);
    std::cout << std::get<0>(check) << std::endl;
    std::cout << std::get<0>(tail(check)) << std::endl;
}

// https://stackoverflow.com/questions/10125211/how-to-create-an-unordered-map-with-non-stl-types-such-as-unicodestring-from-icu
namespace std
{
    template<>
    class hash<UnicodeString> {
    public:
        size_t operator()(const UnicodeString &s) const 
        {
            return (size_t) s.hashCode();
        }
    };
};

void build_unicode_set() {
    std::unordered_set<UnicodeString> check;
    check.emplace(UnicodeString("banana"));
    check.emplace(UnicodeString("apple"));
    check.emplace(UnicodeString("orange"));
    for (const UnicodeString& cur : check) {
        std::string tmp;
        cur.toUTF8String(tmp);
        std::cout << tmp << std::endl;
    }
    if (check.find(UnicodeString("apple")) != check.end()) {
        std::cout << "Found apple" << std::endl;
    } else {
        std::cout << "Did not find apple" << std::endl;
    }
}

struct A {
    const std::string& val;
};

void check_constref_copy() {
    std::string s1{"banana"};
    std::string s2{"banana"};

    std::cout << "String comparison: " << (s1 == s2) << std::endl;
    std::cout << "Address comparison: " << (&s1 == &s2) << std::endl;

    A a1{s1};
    A a2{s2};
    // invoke copy constructor
    A a3{a1};

    std::cout << "a1.val == a2.val: " << (a1.val == a2.val) << std::endl;
    std::cout << "&(a1.val) == &(a2.val): " << (&(a1.val) == &(a2.val)) << std::endl;
    std::cout << "a1.val == a3.val: " << (a1.val == a3.val) << std::endl;
    std::cout << "&(a1.val) == &(a3.val): " << (&(a1.val) == &(a3.val)) << std::endl;

    // based on output from clang-5.0, a const reference member that is copied
    // between two instantiations of an object are exactly the same member.
    // TODO but is this behavior according to spec?
}

int main() {
    // check_translit();
    // list_translit_ids();
    // check_graph_counter();
    // check_crazy();
    // try_compress();
    // try_tuple_tails();
    // build_unicode_set();
    check_constref_copy();
    return 0;
}
