#include<fstream>
#include<iostream>
#include<map>
#include<memory>
#include<string>
#include<tuple>
#include<unordered_map>
#include<unordered_set>
#include<vector>

#include<unicode/uclean.h>
#include<unicode/utypes.h>

#include "tess.hpp"

int main(int argc, char* argv[]) {
    // TODO use real option parser
    if (argc < 4) {
        std::cout << "USAGE: main <source> <target> <cereal>" << std::endl;
        return 1;
    }
    std::string source_path{argv[1]};
    auto locale_us = Locale::getUS();
    auto source_parser{tess::TessParser(locale_us)};
    tess::TessDOM source_dom = tess::read_file(source_path, source_parser);

    std::string cereal_path{argv[3]};
    tess::Transformer transformer{"Latin-Classical", tess::Transformer::LATIN_RULES};
    tess::LatinLemmatizer lemmatizer{cereal_path, transformer};
    auto processors = std::make_tuple(
        std::ref(lemmatizer)
    );
    // TODO why are double repeated features not weighted higher?  Is there a
    // mechanism in v3 that prevents words of only length 1 to not be
    // considered?
    std::vector<tess::Feature> source_features = extract_features(source_dom, processors);
    std::unordered_map<std::string, std::vector<tess::Feature>> source_features_index = index_features(source_features);

    std::string target_path{argv[2]};
    auto target_parser{tess::TessParser(locale_us)};
    tess::TessDOM target_dom = tess::read_file(target_path, target_parser);
    std::vector<tess::Feature> target_features = extract_features(target_dom, processors);

    /*
    for (const tess::TessLine& line : target_dom.lines) {
        const auto& extracted_line = line.parsed;
        const auto& tag = line.tag;
        for (const auto& extracted_prefeature : extracted_line) {
            std::string tmp;
            extracted_prefeature.seq.toUTF8String(tmp);
            std::cout << "Block:\t" << tmp << std::endl;
            std::cout << "Location:\t" << tag << "\t" << extracted_prefeature.start << "\t" << extracted_prefeature.count << std::endl;
        }
    }

    for (const tess::Feature& feat : target_features) {
        std::string tmp;
        feat.token.seq.toUTF8String(tmp);
        std::cout << tmp << "\t" << feat.repr << "\t";
        std::cout << feat.line.tag << "\t" << feat.token.start << "\t";
        std::cout << feat.token.count << std::endl;
    }
    */

    std::unordered_map<std::string, double> source_freqs = calculate_frequencies(source_features);
    std::unordered_map<std::string, double> target_freqs = calculate_frequencies(target_features);
    std::unordered_set<std::string> stopwords = tess::calculate_stopwords_both(10, source_freqs, target_freqs);
    std::vector<tess::Comparanda> comparanda = find_comparanda(source_features_index, target_features, stopwords);

    /*
    std::map<int, int> hist;
    for (const auto& comparandum : comparanda) {
        hist[comparandum.matches.size()]++;
    }
    for (const auto [feature_count, comp_count] : hist) {
        std::cout << feature_count << "\t" << comp_count << std::endl;
    }
    */
    std::vector<std::pair<std::string, double>> ordered_freqs(target_freqs.begin(), target_freqs.end());
    std::sort(ordered_freqs.begin(), ordered_freqs.end(),
        [](const std::pair<std::string, double>& a, const std::pair<std::string, double>& b) {
            return a.second > b.second;
        });
    std::ofstream targfh("target_freqs.txt");
    for (const auto& [word, freq] : ordered_freqs) {
        targfh << word << "\t" << freq << std::endl;
    }

    std::ofstream stopfh("stopwords.txt");
    std::set<std::string> ordered_stopwords(stopwords.begin(), stopwords.end());
    for (const std::string& word : ordered_stopwords) {
        stopfh << word << "\t" << ((source_freqs[word] + target_freqs[word]) / 2.0) << std::endl;
    }

    std::vector<std::pair<const tess::Comparanda*, double>> to_sort;
    for (const auto& comparandum : comparanda) {
        to_sort.emplace_back(std::make_pair(&comparandum, tess::score_comparanda(comparandum, source_freqs, target_freqs)));
    }
    std::sort(to_sort.begin(), to_sort.end(),
        [](const std::pair<const tess::Comparanda*, double>& a, const std::pair<const tess::Comparanda*, double>& b) {
            return a.second > b.second;
        });

    // TODO instead, try building function that outputs list of results from
    // TessDOM?  If so, what parts are reusable?
    std::ofstream ofh("comparanda.txt");
    for (const auto& [comp, score] : to_sort) {
        auto& comparandum = *comp;
        if (comparandum.matches.size() >= 2) {
            ofh << comparandum.source_line.tag << "\t" << comparandum.source_line.raw << "\t";
            ofh << comparandum.target_line.tag << "\t" << comparandum.target_line.raw << "\t";
            for (const auto& match : comparandum.matches) {
                ofh << match.source_position << ":" << match.target_position << "(";
                for (const std::string& repr : match.reprs) {
                    ofh << repr << ",";
                }
                ofh << ");";
            }
            ofh << "\t" << score << std::endl;
        }
    }

    u_cleanup();
    return 0;
}
