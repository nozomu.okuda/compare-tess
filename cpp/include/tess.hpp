#pragma once
#include<algorithm>
#include<cmath>
#include<fstream>
#include<iostream>
#include<limits>
#include<map>
#include<memory>
#include<set>
#include<string>
#include<tuple>
#include<unordered_map>
#include<unordered_set>
#include<stdexcept>
#include<vector>

#include<unicode/brkiter.h>
#include<unicode/regex.h>
#include<unicode/translit.h>
#include<unicode/uclean.h>
#include<unicode/unistr.h>
#include<unicode/utypes.h>

#include "cereal/archives/binary.hpp"
#include "cereal/types/string.hpp"
#include "cereal/types/unordered_map.hpp"
#include "cereal/types/unordered_set.hpp"

namespace tess {
    /**
     * Grapheme sequence with associated position information
     *
     * To be used with text processing pipeline so that final features
     * extracted from pipeline can be mapped back to grapheme positions in
     * original input text
     */
    struct PreFeature {
        const UnicodeString seq;
        const std::size_t start;
        const std::size_t count;

        PreFeature() = default;
        PreFeature(const UnicodeString& in_seq, const std::size_t in_start, const std::size_t in_count) :
            seq(in_seq), start(in_start), count(in_count) {}
    };

    std::ostream& operator <<(std::ostream& strm, const PreFeature& prefeat) {
        std::string tmp;
        prefeat.seq.toUTF8String(tmp);
        return strm << tmp << "\t" << prefeat.start << "\t" << prefeat.count;
    }

    /**
     * Represents a line in a .tess file
     *
     * Each line has a tag and associated data; each associated data is
     * comprised of PreFeatures
     */
    struct TessLine {
        const std::string tag;
        const std::string raw;
        const std::vector<PreFeature> parsed;

        TessLine() = default;
        TessLine(const std::string& in_tag, const std::string& in_raw, const std::vector<PreFeature>& in_parsed) :
            tag(in_tag), raw(in_raw), parsed(in_parsed) {}

        bool operator==(const TessLine& other) const {
            // TODO reconsider this implementation
            // if I instantiate multiple TessLines of the same line, how can I
            // say that they are all the same?
            // how do I handle the case of exactly repeated lines?
            // do I have to rely on tag being unique?
            return this == &other;
        }
    };

    /**
     * Represents document object model of a .tess file
     */
    struct TessDOM {
        const std::vector<TessLine> lines;

        TessDOM() = default;
        TessDOM(const std::vector<TessLine>& in_lines) : lines(in_lines) {}
    };

    /**
     * Represents an extracted feature
     *
     * Each feature has a representation by which to be compared.
     * Each feature is derived from some token in a TessLine.
     * Each token comes from a TessLine.
     * Each source has an associated position in its associated TessLine.
     */
    struct Feature {
        const std::string repr;
        const PreFeature& token;
        const TessLine& line;
        const std::size_t position;

        Feature() = default;
        Feature(const std::string& in_repr, const PreFeature& in_token, const TessLine& in_line, const std::size_t in_position):
            repr(in_repr), token(in_token), line(in_line), position(in_position) {}
    };
}

// https://stackoverflow.com/questions/10125211/how-to-create-an-unordered-map-with-non-stl-types-such-as-unicodestring-from-icu
namespace std
{
    template<>
    struct hash<UnicodeString> {
    public:
        size_t operator()(const UnicodeString& s) const {
            return (size_t) s.hashCode();
        }
    };

    template<>
    struct hash<tess::TessLine> {
    public:
        size_t operator()(const tess::TessLine& line) const {
            return hash<std::string>{}(line.tag);
        }
    };

    template<>
    struct hash<tess::Feature> {
    public:
        size_t operator()(const tess::Feature& feat) const {
            return hash<std::string>{}(feat.repr);
        }
    };
};

namespace tess {
    // http://blogorama.nerdworks.in/iteratingoverastdtuple/
    template<size_t N, typename... Ts>
    struct proc_apply_iter {
        static std::vector<UnicodeString> process(std::tuple<Ts...>& processors, const std::vector<UnicodeString>& input) {
            return std::get<N>(processors)(proc_apply_iter<N-1, Ts...>::process(processors, input));
        }
    };

    template<typename... Ts>
    struct proc_apply_iter<0, Ts...> {
        static std::vector<UnicodeString> process(std::tuple<Ts...>& processors, const std::vector<UnicodeString>& input) {
            return std::get<0>(processors)(input);
        }
    };

    template<typename... Ts>
    std::vector<UnicodeString> run_processors(std::tuple<Ts...>& processors, const std::vector<UnicodeString>& input) {
        return proc_apply_iter<std::tuple_size<std::tuple<Ts...>>::value - 1, Ts...>::process(processors, input);
    }

    std::unordered_map<std::string, double> calculate_frequencies(const std::vector<Feature>& features) {
        double total_count = 0;
        std::unordered_map<std::string, double> result;
        for (const Feature& feat : features) {
            // Confirmed (11 Jan 2018):  Every feature is counted in
            // calculating the frequency for stopword purposes
            //
            // Load stopwords list stats from pregenerated files
            // https://github.com/tesserae/tesserae/blob/23092131514d851098f88d39a20915e0c1c5500e/cgi-bin/read_table.pl#L1119-L1129
            //
            // Generate stopwords list stats for lemmatized texts
            // https://github.com/tesserae/tesserae/blob/23092131514d851098f88d39a20915e0c1c5500e/scripts/v3/add_col_stem.pl#L276
            //
            // Write out stopwords list stats to proper location in proper
            // format
            // https://github.com/tesserae/tesserae/blob/23092131514d851098f88d39a20915e0c1c5500e/scripts/TessPerl/Tesserae.pm#L690-L691
            //
            // But is behavior of score frequencies wrong or correct?  I'm
            // going to assume wrong.
            result[feat.repr]++;
            ++total_count;
        }
        for (const auto& [word, count] : result) {
            result[word] = count / total_count;
        }
        return result;
    }

    std::unordered_set<std::string> calculate_stopwords_both(const std::size_t number, const std::unordered_map<std::string, double>& source_freqs, const std::unordered_map<std::string, double>& target_freqs) {
        std::vector<std::pair<std::string, double>> tmp;
        for (const auto& [word, freq] : source_freqs) {
            auto got = target_freqs.find(word);
            if (got != target_freqs.end()) {
                tmp.emplace_back(std::make_pair(word, (freq + got->second) / 2.0));
            }
        }
        std::sort(tmp.begin(), tmp.end(),
            [](const std::pair<std::string, double>& a, const std::pair<std::string, double>& b) {
                return a.second > b.second;
            });
        std::unordered_set<std::string> result;
        std::size_t i = 0;
        for (const auto& [word, _] : tmp) {
            result.emplace(word);
            ++i;
            if (i >= number) break;
        }
        return result;
    }

    /**
     * Wrapper class for ICU's rule-based Transliterator
     */
    class Transformer {
        // maintainer beware:  declaration order of members is important to
        // constructor
        UErrorCode my_status;
        UParseError my_parse_status;
        std::unique_ptr<Transliterator> converter;

    public:
        // if you're getting an error that leads here, you're probably passing
        // types to Transformer's constructor that
        // Transliterator::createFromRules doesn't like
        template<typename T, typename U>
        Transformer(const T& name, const U& rules):
            my_status(U_ZERO_ERROR),
            converter(Transliterator::createFromRules(
                name, rules, UTRANS_FORWARD,
                this->my_parse_status, this->my_status)) {}

        UnicodeString operator()(const UnicodeString& input) const {
            UnicodeString copy(input);
            this->converter->transliterate(copy);
            return copy;
        }

        // Documentation for building transliterator rules:
        // http://userguide.icu-project.org/transforms/general
        constexpr static const char* LATIN_RULES{":: NFD; :: [:M:] Remove; :: lower; j > i; v > u; :: NFKC;"};
        constexpr static const char* LATIN_DICTIONARY_RULES{":: NFD; :: [:^L:] Remove; :: lower; j > i; v > u; :: NFKC;"};
        // TODO make GREEK_RULES
        constexpr static const char* GREEK_RULES{":: NFD; :: [:M:] Remove; :: lower; :: NFKC;"};
    };

    /**
     * Makes grapheme counting easier
     *
     * ICU documentation suggests not making too many BreakIterators, which is
     * what this class wraps
     */
    class GraphemeCounter {
        // maintainer beware:  declaration order of members is important to
        // constructor
        UErrorCode my_status;
        std::unique_ptr<BreakIterator> my_iter;

    public:
        GraphemeCounter(const Locale& where):
            my_status(U_ZERO_ERROR),
            my_iter(BreakIterator::createCharacterInstance(where, this->my_status)){}

        size_t operator()(const UnicodeString& text) {
            this->my_iter->setText(text);
            int32_t start_boundary = this->my_iter->first();
            if (start_boundary == BreakIterator::DONE) {
                return 0;
            }
            size_t result = 0;
            // assuming that if there is a start boundary, then there will be
            // an end boundary
            int32_t end_boundary = this->my_iter->next();
            while (end_boundary != BreakIterator::DONE) {
                ++result;
                start_boundary = end_boundary;
                end_boundary = this->my_iter->next();
            }
            return result;
        }
    };

    /**
     * Separates text at word breaks
     */
    class WordBreaker {
        // maintainer beware:  declaration order of members is important to
        // constructor
        UErrorCode my_breaker_status;
        std::unique_ptr<BreakIterator> breaker;
        UErrorCode my_matcher_status;
        RegexMatcher matcher;
        GraphemeCounter grapheme_counter;

    public:
        WordBreaker(const Locale& where):
            my_breaker_status(U_ZERO_ERROR),
            breaker(BreakIterator::createWordInstance(where, this->my_breaker_status)),
            my_matcher_status(U_ZERO_ERROR),
            // this regex matches on a sequence of Unicode letters and marks
            matcher("^[\\p{L}\\p{M}]+", 0, this->my_matcher_status),
            grapheme_counter(where){}

        // TODO use streaming framework/library?
        std::vector<PreFeature> operator()(const UnicodeString& input) {
            std::vector<PreFeature> result;
            this->breaker->setText(input);
            int32_t start_boundary = this->breaker->first();
            if (start_boundary == BreakIterator::DONE) {
                // empty data
                return result;
            }
            // assuming that if there is a start boundary, there will always be
            // an end boundary
            int32_t end_boundary = this->breaker->next();
            size_t grapheme_count = 0;
            while (end_boundary != BreakIterator::DONE) {
                size_t start_pos = grapheme_count;
                UnicodeString inter_bounds;
                input.extract(start_boundary, end_boundary-start_boundary, inter_bounds);
                grapheme_count += this->grapheme_counter(inter_bounds);
                this->matcher.reset(inter_bounds);
                if (this->matcher.matches(this->my_matcher_status)) {
                    result.emplace_back(
                        inter_bounds,
                        start_pos,
                        grapheme_count - start_pos);
                }
                start_boundary = end_boundary;
                end_boundary = this->breaker->next();
            }
            return result;
        }
    };

    class LatinLemmatizer {
        const Transformer& transformer;
        std::unordered_map<std::string, std::unordered_set<std::string>> lookup;

    public:
        LatinLemmatizer(const std::string& filepath, const Transformer& transformer):
                transformer(transformer) {
            std::ifstream is(filepath, std::ios::binary);
            cereal::BinaryInputArchive iarchive(is);
            iarchive(this->lookup);
        }

        std::vector<UnicodeString> operator()(const std::vector<UnicodeString>& input) {
            std::vector<UnicodeString> result;
            for (const UnicodeString& cur : input) {
                UnicodeString transformed(this->transformer(cur));
                std::string query;
                transformed.toUTF8String(query);
                auto got = this->lookup.find(query);
                if (got != this->lookup.end()) {
                    for (const std::string& lemma : got->second) {
                        result.emplace_back(lemma.c_str());
                    }
                }
            }
            return result;
        }
    };

    template<typename... Ts>
    std::vector<Feature> extract_features(const TessDOM& input, std::tuple<Ts...>& processors) {
        std::vector<Feature> result;
        for (const TessLine& line : input.lines) {
            for (size_t i = 0; i < line.parsed.size(); ++i) {
                const PreFeature& prefeat = line.parsed[i];
                std::vector<UnicodeString> processed{run_processors(
                    processors, std::vector<UnicodeString>{prefeat.seq})};
                for (const UnicodeString& icustr : processed) {
                    std::string tmp;
                    icustr.toUTF8String(tmp);
                    result.emplace_back(tmp, prefeat, line, i);
                }
            }
        }
        return result;
    }

    std::unordered_map<std::string, std::vector<Feature>> index_features(const std::vector<Feature>& features) {
        std::unordered_map<std::string, std::vector<Feature>> result;
        for (const Feature& feat : features) {
            result[feat.repr].emplace_back(feat);
        }
        return result;
    }

    struct Match {
        // TODO redeclare with SourcePosition and TargetPosition types to
        // prevent confusion
        const std::size_t source_position;
        const std::size_t target_position;
        const std::set<std::string> reprs;

        Match() = default;
        Match(const std::size_t in_source_pos, const std::size_t in_target_pos, const std::set<std::string>& in_reprs):
            source_position(in_source_pos),
            target_position(in_target_pos),
            reprs(in_reprs) {}
    };

    /**
     * Represents a TessLine with associated Features worth considering in a
     * comparison
     */
    struct Comparanda {
        const TessLine& source_line;
        const TessLine& target_line;
        std::vector<Match> matches;

        Comparanda() = default;
        Comparanda(
                const TessLine& in_source,
                const TessLine& in_target,
                const std::vector<Match>& in_matches):
            source_line(in_source),
            target_line(in_target),
            matches(in_matches) {}
    };

    std::vector<Comparanda> find_comparanda(
            const std::unordered_map<std::string, std::vector<Feature>>& index,
            const std::vector<Feature>& features,
            const std::unordered_set<std::string>& stopwords) {
        std::unordered_map<const TessLine*, std::unordered_map<const TessLine*, std::map<std::size_t, std::map<std::size_t, std::set<std::string>>>>> t_line2s_line2t_pos2s_pos2reprs;
        for (const Feature& target_feat : features) {
            auto got = index.find(target_feat.repr);
            if (got != index.end()) {
                for (const Feature& source_feat : got->second) {
                    t_line2s_line2t_pos2s_pos2reprs[&(target_feat.line)][&(source_feat.line)][target_feat.position][source_feat.position].emplace(target_feat.repr);
                }
            }
        }

        std::vector<Comparanda> result;
        for (const auto& [target_line, s_line2t_pos2s_pos2reprs] : t_line2s_line2t_pos2s_pos2reprs) {
            for (const auto& [source_line, t_pos2s_pos2reprs] : s_line2t_pos2s_pos2reprs) {
                std::vector<Match> cur_matches;
                for (const auto& [target_pos, s_pos2reprs] : t_pos2s_pos2reprs) {
                    for (const auto& [source_pos, reprs] : s_pos2reprs) {
                        for (const auto& repr : reprs) {
                            if (stopwords.count(repr) == 0) {
                                cur_matches.emplace_back(source_pos, target_pos, reprs);
                                break;
                            }
                        }
                    }
                }
                result.emplace_back(*source_line, *target_line, cur_matches);
            }
        }
        return result;
    }

    std::vector<std::pair<const Match*, double>> get_sorted_matches(const std::vector<Match>& matches, const std::unordered_map<std::string, double>& freqs) {
        std::vector<std::pair<const Match*, double>> result;
        for (const Match& match : matches) {
            double lowest = std::numeric_limits<double>::infinity();
            for (const std::string& repr : match.reprs) {
                double cur = freqs.at(repr);
                if (cur < lowest) lowest = cur;
            }
            result.emplace_back(&match, lowest);
        }
        std::sort(result.begin(), result.end(),
            [](const std::pair<const Match*, double>& a, const std::pair<const Match*, double>& b) {
                return a.second < b.second;
            });
        return result;
    }

    std::size_t calculate_source_distance(const std::vector<Match>& matches, const std::unordered_map<std::string, double>& freqs) {
        if (matches.size() == 2) {
            if (matches[0].source_position > matches[1].source_position) {
                return matches[0].source_position - matches[1].source_position;
            }
            return matches[1].source_position - matches[0].source_position;
        }
        std::vector<std::pair<const Match*, double>> sorted = get_sorted_matches(matches, freqs);

        if (sorted[0].first->source_position > sorted[1].first->source_position) {
            return sorted[0].first->source_position - sorted[1].first->source_position;
        }
        return sorted[1].first->source_position - sorted[0].first->source_position;
    }

    std::size_t calculate_target_distance(const std::vector<Match>& matches, const std::unordered_map<std::string, double>& freqs) {
        if (matches.size() == 2) {
            if (matches[0].target_position > matches[1].target_position) {
                return matches[0].target_position - matches[1].target_position;
            }
            return matches[1].target_position - matches[0].target_position;
        }
        std::vector<std::pair<const Match*, double>> sorted = get_sorted_matches(matches, freqs);

        if (sorted[0].first->target_position > sorted[1].first->target_position) {
            return sorted[0].first->target_position - sorted[1].first->target_position;
        }
        return sorted[1].first->target_position - sorted[0].first->target_position;
    }

    double score_comparanda(const Comparanda& comp, const std::unordered_map<std::string, double>& source_freqs, const std::unordered_map<std::string, double>& target_freqs) {
        if (comp.matches.size() < 2) return 0.0;
        std::size_t source_distance = calculate_source_distance(comp.matches, source_freqs);
        std::size_t target_distance = calculate_target_distance(comp.matches, target_freqs);
        double source_term = 0.0;
        double target_term = 0.0;
        for (const Match& match : comp.matches) {
            // v3 pre-sums the lemma counts in its frequency table, so we have
            // to do the same here
            double source_tmp = 0.0;
            double target_tmp = 0.0;
            for (const std::string& repr : match.reprs) {
                if (source_freqs.count(repr) > 0) source_tmp += source_freqs.at(repr);
                if (target_freqs.count(repr) > 0) target_tmp += target_freqs.at(repr);
            }
            if (source_tmp > 0) source_term += 1.0 / source_tmp;
            if (target_tmp > 0) target_term += 1.0 / target_tmp;
        }
        return std::log((source_term + target_term) / (source_distance + target_distance));
    }

    // TODO put in extractors.hpp
    /**
     * Builds a document object model of a .tess file
     *
     * Once process() has been called on an instance of this class, that
     * instance can no longer accept new input
     */
    class TessParser {
        // maintainer beware:  declaration order of members is important to
        // constructor
        UErrorCode parser_status;
        RegexMatcher file_parser;
        GraphemeCounter grapheme_counter;
        WordBreaker breaker;
        std::vector<std::string> extracted_tags;
        std::vector<std::string> extracted_raw_data;
        std::vector<std::vector<PreFeature>> extracted_data;

    public:
        TessParser(const Locale& where):
            parser_status(U_ZERO_ERROR),
            file_parser("^\\S*<(.+)>\\s+(.+)", 0, this->parser_status),
            grapheme_counter(where),
            breaker(where) {}

        TessParser& operator<<(const UnicodeString& input) {
            this->file_parser.reset(input);
            if (this->file_parser.matches(this->parser_status)) {
                UnicodeString tag = this->file_parser.group(1, this->parser_status);
                std::string converted_tag;
                tag.toUTF8String(converted_tag);
                this->extracted_tags.emplace_back(converted_tag);

                UnicodeString raw_data = this->file_parser.group(2, this->parser_status);
                std::string converted_raw_data;
                raw_data.toUTF8String(converted_raw_data);
                this->extracted_raw_data.emplace_back(converted_raw_data);

                this->extracted_data.emplace_back(breaker(raw_data));
            }
            return *this;
        }

        TessDOM process() {
            std::vector<TessLine> result;
            for (int i = 0; i < extracted_data.size(); ++i) {
                result.emplace_back(extracted_tags[i], extracted_raw_data[i], extracted_data[i]);
            }
            return TessDOM{result};
        }
    };

    /**
     * Extracts lemma information from .csv dictionaries
     *
     * Once process() has been called on an instance of this class, that
     * instance can no longer accept new input
     */
    class DictionaryParser {
        UErrorCode parser_status;
        RegexMatcher file_parser;
        const Transformer& transformer;
        std::unordered_map<std::string, std::unordered_set<std::string>> data;

    public:
        DictionaryParser(const Transformer& transformer):
            parser_status(U_ZERO_ERROR),
            file_parser("^(.+),(.+),(.+)(,.+)*", 0, this->parser_status),
            transformer(transformer) {}

        DictionaryParser& operator<<(const UnicodeString& input) {
            this->file_parser.reset(input);
            if (this->file_parser.matches(this->parser_status)) {
                UnicodeString token = this->file_parser.group(1, this->parser_status);
                UnicodeString headword = this->file_parser.group(3, this->parser_status);
                UnicodeString transformed_token(this->transformer(token));
                UnicodeString transformed_headword(this->transformer(headword));

                std::string str_token;
                transformed_token.toUTF8String(str_token);
                std::string str_headword;
                transformed_headword.toUTF8String(str_headword);

                // since bracket access creates empty value if key does not
                // exist, the following line happens to do what we want it to
                // do
                this->data[str_token].emplace(str_headword);
            }
            return *this;
        }

        std::unordered_map<std::string, std::unordered_set<std::string>> process() {
            return this->data;
        }
    };

    // TODO add error checking on T
    /**
     * Reads file line by line, applying parser T to each line
     *
     * Assumes that file is UTF-8 encoded
     *
     * T must implement the input operator for UnicodeString; T must also
     * implement the process() method
     */
    template<typename T>
    auto read_file(const std::string& filepath, T& parser) {
        std::ifstream ifh(filepath);
        for (int line_count = 0; ifh.good(); ++line_count) {
            std::string cur;
            std::getline(ifh, cur);

            UnicodeString line = UnicodeString::fromUTF8(StringPiece(cur.c_str()));
            parser << line;
        }
        return parser.process();
    }
}
