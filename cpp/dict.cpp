#include<fstream>
#include<string>
#include<unordered_map>
#include<unordered_set>

#include<unicode/uclean.h>
#include<unicode/utypes.h>

#include "cereal/archives/binary.hpp"
#include "cereal/types/string.hpp"
#include "cereal/types/unordered_map.hpp"
#include "cereal/types/unordered_set.hpp"

#include "tess.hpp"

int main(int argc, char* argv[]) {
    // TODO use real option parser
    if (argc < 3) {
        std::cout << "USAGE: main <dict.csv> <bin.cereal>" << std::endl;
        return 1;
    }
    std::string source_path(argv[1]);
    tess::Transformer transformer("Latin-Classical", tess::Transformer::LATIN_DICTIONARY_RULES);
    tess::DictionaryParser dict_parser(transformer);
    std::unordered_map<std::string, std::unordered_set<std::string>> lookup = tess::read_file(source_path, dict_parser);
    /*
    for (const auto& [key, value] : lookup) {
        std::cout << key << std::endl;
        for (const auto& str : value) {
            std::cout << "\t" << str << std::endl;
        }
    }
    */
    std::string outbin(argv[2]);
    std::ofstream os(argv[2], std::ios::binary);
    cereal::BinaryOutputArchive oarchive(os);
    oarchive(lookup);
    u_cleanup();
    return 0;
}
