#include<fstream>
#include<string>
#include<unordered_map>
#include<unordered_set>

#include<unicode/uclean.h>
#include<unicode/utypes.h>

#include "cereal/archives/binary.hpp"
#include "cereal/types/string.hpp"
#include "cereal/types/unordered_map.hpp"
#include "cereal/types/unordered_set.hpp"

#include "tess.hpp"

int main(int argc, char* argv[]) {
    // TODO use real option parser
    if (argc < 2) {
        std::cout << "USAGE: main <bin.cereal>" << std::endl;
        return 1;
    }
    std::string inbin(argv[1]);
    std::unordered_map<std::string, std::unordered_set<std::string>> lookup;
    std::ifstream is(inbin, std::ios::binary);
    cereal::BinaryInputArchive iarchive(is);
    iarchive(lookup);
    for (const auto& [key, value] : lookup) {
        std::cout << key << std::endl;
        for (const auto& str : value) {
            std::cout << "\t" << str << std::endl;
        }
    }
    return 0;
}
