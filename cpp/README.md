# Tesserae C++

This is an implementation of the Tesserae algorithm in C++.  It is conceived of as a way to see whether using lower level constructs produces significantly faster execution speed.

## Prerequisites

Although possible for this code to compile correctly in many different environments, here's the one I use (and the only one I can guarantee this code working in):
* Ubuntu 16.04
* clang++-5.0 (clang-5.0)
* icu 55.1 (libicu-dev, icu-devtools)
* zlib 1.2.8 (zlib1g-dev)

The parenthesized expressions are the library names on apt.

In addition, it is assumed that [Cereal](https://github.com/USCiLab/cereal) is cloned to the `${HOME}/Code` directory.  I was using commit `51cbda5f30e56c801c07fe3d3aba5d7fb9e6cca4` when running the code on my machine.

## Generating Necessary Data Files

Make sure that you have the Latin lexicon file in the appropriate directory.  The easiest way to do this is to clone [Tesserae](https://github.com/tesserae/tesserae) into `${HOME}/Code`.

In this directory (where this README.md file is), run:
`make dict`

## Running Code

In this directory, compile the code by running:
`make compile`


To run the code in this directory:
`./main <source> <target> latin_dict.cereal`
